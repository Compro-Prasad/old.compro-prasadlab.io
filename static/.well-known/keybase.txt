==================================================================
https://keybase.io/compro
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://compro-prasad.gitlab.io
  * I am compro (https://keybase.io/compro) on keybase.
  * I have a public key ASAbZ-8jCEIxy81B9zVVjD4UY2EbETL8yK0LA-HO6YqCFQo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "01201b67ef23084231cbcd41f735558c3e1463611b1132fcc8ad0b03e1cee98a82150a",
      "host": "keybase.io",
      "kid": "01201b67ef23084231cbcd41f735558c3e1463611b1132fcc8ad0b03e1cee98a82150a",
      "uid": "315da70d4150154979057e2361767219",
      "username": "compro"
    },
    "merkle_root": {
      "ctime": 1543986076,
      "hash": "5d81cb57a45f37a9457e4a764f4c3fdb255a54750123c50b6eb67462281d1d75e7fcef08261aceca5f17e4c76cfdf48f9ae51cddad3323b04894c355bf18526e",
      "hash_meta": "91946e0204923b3515e749a45f56517f07d27b96d80cd54a601b22acd42f0fec",
      "seqno": 4054179
    },
    "service": {
      "entropy": "xSXXJEXkfFQyRg+ycc2T+u9F",
      "hostname": "compro-prasad.gitlab.io",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "2.8.0"
  },
  "ctime": 1543986422,
  "expire_in": 504576000,
  "prev": "1a5c0222440e07caebf1f44391fddc56d343acaba31eeb797bf336db39543340",
  "seqno": 7,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgG2fvIwhCMcvNQfc1VYw+FGNhGxEy/MitCwPhzumKghUKp3BheWxvYWTESpcCB8QgGlwCIkQOB8rr8fRDkf3cVtNDrKujHut5e/M22zlUM0DEILQY7wl+bRV6zSwwh9dqnPmu+pvEj3DvMBDfqRI5R+LgAgHCo3NpZ8RAx64oAG6zPFV8IPxsWezxC0G7n33m6FS4erRvOK51TfHVLKpNmvA5Prz2I848KgZnOYvPmGjQHfzzhL8Cid/SDqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIOEhPH1C72ZDLdJc56h4PiYRNxeqUQnTexWlFHRbiWgUo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/compro

==================================================================

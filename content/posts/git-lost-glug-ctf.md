+++
title = "Git l0st"
author = ["Compro Prasad"]
date = 2017-11-08T21:34:38-05:30
draft = false
+++

I was trying to do the high bounty questions in each section in GLUG CTF. This
question was in the _Miscellaneous_ section. You can download the [zip](/ox-hugo/Project.zip) to try it
out yourself.

Unzip it first. Its basically a Git repository.

```shell
unzip Project.zip
```

See the contents in `.git/objects/??/*`. To get all hashes run:

```shell
ls .git/objects/??/* | awk -F / '{print $(NF-1) $NF}'
```

Use the script given below to check which of the hashes represent commits:

```shell
  for sha in $(ls .git/objects/??/* | awk -F / '{print $(NF-1) $NF}')
  do
      echo -n "$sha "
      git cat-file -t $sha
  done
```

Doing `git log` reveals that one of the commits was deleted. We can recover that
using `git checkout 9ee03be7cd`. This commit brings in a new file `index2` which
has the flag in it.

Hope you can feed it to the cat so that it can tell you the flag :)

**Edit 1:** To get the deleted commit hash you can use `git fsck --lost-found`
 which is far more simpler option than what is mentioned above.
